/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.apassara.cuckcoohashing;

/**
 *
 * @author ASUS
 */
public class Cuckcoo {

    int n; //size in table
    final int numOftable = 2; // Number of tables default = 2
    Node[][] Tb;
    int CountNode = 0;

    public Cuckcoo(int size) {
        this.n = size;
        Tb = new Node[numOftable][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < numOftable; j++) {
                Tb[j][i] = null; // new Node
            }
        }
    }
   
    // Default 
    public Cuckcoo() {
        this.n = 23; // Prime number 
        Tb = new Node[numOftable][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < numOftable; j++) {
                Tb[j][i] = null;
            }
        }
    }

    private int getHashIndex(int table, int key) {
        switch (table) {
            case 1 -> {
                return key % n;
            }
            case 2 -> {
                return (key / n) % n;
            }
        }
        return 0;
    }

    public void put(int key, String value) {
        if (Tb[0][getHashIndex(1, key)] != null && Tb[0][getHashIndex(1, key)].key == key) {
            Tb[0][getHashIndex(1, key)].value = value;
            return;
        }
        if (Tb[1][getHashIndex(2, key)] != null && Tb[1][getHashIndex(2, key)].key == key) {
            Tb[1][getHashIndex(2, key)].value = value;
            return;
        }

        int Key = 0;
        String Value = "";

        int i = 0;
        int cycleOccur = 0;
        do {
            if (Tb[i][getHashIndex(i + 1, key)] == null) {
                Tb[i][getHashIndex(i + 1, key)] = new Node(key, value) {};
                CountNode++;
                return;
            }
            Node temp = Tb[i][getHashIndex(i + 1, key)];
            Tb[i][getHashIndex(i + 1, key)] = new Node(key, value);
            key = temp.key;
            value = temp.value;
            cycleOccur++;
            i = (i + 1) % 2;
            if (cycleOccur + 1 == n) {
                Key = Tb[i][getHashIndex(i + 1, key)].key;
                Value = Tb[i][getHashIndex(i + 1, key)].value;
            }
        } while (cycleOccur < CountNode + 1);

        if (cycleOccur == n) {
            System.out.println("REHASH Following input cause a cycle.");
            System.out.println(Key + " , " + Value + " not assign to array.\n");
        }

    }

    public String get(int key) {
        if (Tb[0][getHashIndex(1, key)] != null && Tb[0][getHashIndex(1, key)].key == key) {
            return Tb[0][getHashIndex(1, key)].value;
        }
        if (Tb[1][getHashIndex(2, key)] != null && Tb[1][getHashIndex(2, key)].key == key) {
            return Tb[1][getHashIndex(2, key)].value;
        }
        return null;
    }

    public void remove(int key) {
        if (Tb[0][getHashIndex(1, key)] != null && Tb[0][getHashIndex(1, key)].key == key) {
            Tb[0][getHashIndex(1, key)] = null;
        }
        if (Tb[1][getHashIndex(2, key)] != null && Tb[1][getHashIndex(2, key)].key == key) {
            Tb[1][getHashIndex(2, key)] = null;
        }
    }

    public void getTable() {
        for (int i = 1; i < numOftable + 1; i++) {
            for (int j = 0; j < n; j++) {
                if (Tb[i - 1][j] != null) {
                    System.out.println("[" + i + "," + j + "] = " + Tb[i - 1][j].key + " , " + Tb[i - 1][j].value);
                } else {
                    System.out.println("[" + i + "," + j + "] = is null");
                }
                System.out.println("");
            }
        }
    }

    public static void main(String[] args) {
        Cuckcoo cc = new Cuckcoo(9);
        cc.put(36, "36");
        cc.put(100, "100");
        cc.put(39, "39");
        cc.put(75, "75");
        cc.put(20, "40");
        cc.put(34, "34");
        cc.put(67, "67");
        cc.put(50, "50");
        cc.put(3, "3");
        cc.getTable();
    }
}


